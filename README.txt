INTRODUCTION
------------

Current Maintainer: WQ Meng <wqmeng@gmail.com>


Relation clone provides support for Relation when using the Node clone module.
With this module enabled, selected relations are copied to the new node when
a node is cloned. This can save time when setting up many similar nodes in a
site.
